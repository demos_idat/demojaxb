package com.demo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "oficina")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Oficina implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private String name;
    
    public Oficina() {}
    
    
    
    
	public Oficina(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}




	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}




	@Override
	public String toString() {
		return "Oficina [id=" + id + ", name=" + name + "]";
	}
    
    
    
}
