package com.demo;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Conversion {
	
	public static Empleado xmlToObject(String pathXML) {
		File xmlFile = new File(pathXML);
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Empleado.class);             
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		    Empleado empleado = (Empleado) jaxbUnmarshaller.unmarshal(xmlFile);
		   return empleado;
		} catch (Exception e) {
			 e.printStackTrace();
			 return null;
		}
	}
	
	public static String objectToXml(Empleado empleado) {
		JAXBContext jaxbContext;
		try {
			File file = new File("/demo/resultado.xml");
			jaxbContext = JAXBContext.newInstance(Empleado.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(empleado, file);
			jaxbMarshaller.marshal(empleado, System.out);
			return "/demo/resultado.xml";
		} catch (Exception e) {
			e.printStackTrace();
			 return null;
		}
	}

}
