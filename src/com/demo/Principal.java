package com.demo;

public class Principal {

	public static void main(String[] args) {
		
		Empleado empleado = Conversion.xmlToObject("/demo/empleado.xml");
		System.out.println(empleado.toString());
		
		Empleado empleado2 = new Empleado(2, "Carlos", "Sanchez", new Oficina(1, "Gerencia"));
		System.out.println(Conversion.objectToXml(empleado2));

	}

}
