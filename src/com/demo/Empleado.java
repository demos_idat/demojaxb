package com.demo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "empleado")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Empleado {
	
	private Integer id;
	private String nombre;
    private String apellido;
    private Oficina oficina;
    
    
    public Empleado() {}


	public Empleado(Integer id, String nombre, String apellido, Oficina oficina) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.oficina = oficina;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public Oficina getOficina() {
		return oficina;
	}


	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}


	@Override
	public String toString() {
		return "Empleado [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", oficina=" + oficina + "]";
	}


	
    
    

}
